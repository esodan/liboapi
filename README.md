# Introduction

This library uses [GVo](https://gitlab.gnome.org/esodan/gvls/), the `GLib.Variant` to `GLib.Object` library,
to create an API to read/write JSON documents for [OpenAPI Specification](https://swagger.io/specification/).


