/* liboapi - OpenAPI implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Oapi.PathItem : GLib.Object, GVo.Object
{
    [Description (nick="$ref")]
    public string reference { get ; set; }

    [Description (nick="sumary")]
    public string sumary { get ; set; }

    [Description (nick="description")]
    public string description { get ; set; }

    [Description (nick="get")]
    public Operation get_operation { get ; set; }

    [Description (nick="put")]
    public Operation put_operation { get ; set; }

    [Description (nick="post")]
    public Operation post_operation { get ; set; }

    [Description (nick="delete")]
    public Operation delete_operation { get ; set; }

    [Description (nick="options")]
    public Operation options_operation { get ; set; }

    [Description (nick="head")]
    public Operation head_operation { get ; set; }

    [Description (nick="patch")]
    public Operation patch_operation { get ; set; }

    [Description (nick="trace")]
    public Operation trace_operation { get ; set; }

    /**
     * A list of {@link Server} objects
     */
    [Description (nick="servers")]
    public GVo.Container servers { get ; set; }

    /**
     * A list of {@link Parameter} or {@link Reference} objects
     */
    [Description (nick="parameters")]
    public GVo.Container parameters { get ; set; }
}
