/* liboapi - OpenAPI implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Oapi.Link : GLib.Object, GVo.Object
{
    [Description (nick="operationRef")]
    public string operation_ref { get ; set; }

    [Description (nick="operationId")]
    public string operation_id { get ; set; }

    /**
     * A map with a string property as a key and objects
     * {@link GLib.Variant} as value or a expression
     */
    [Description (nick="parameters")]
    public GVo.Container parameters { get ; set; }

    /**
     * A {@link GLib.Variant} or a expression
     */
    [Description (nick="requestBody")]
    public GLib.Variant request_body { get ; set; }

    [Description (nick="description")]
    public string description { get ; set; }

    [Description (nick="server")]
    public Server server { get ; set; }
}
