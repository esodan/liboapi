/* liboapi - OpenAPI implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * [[https://swagger.io/specification/#components-object]]
 */
public class Oapi.Components : GLib.Object, GVo.Object
{
    [Description (nick="schemas")]
    public GVo.Container schemas { get ; set; }

    [Description (nick="responses")]
    public GVo.Container responses { get ; set; }

    [Description (nick="parameters")]
    public GVo.Container parameters { get ; set; }

    [Description (nick="examples")]
    public GVo.Container examples { get ; set; }

    [Description (nick="requestBodies")]
    public GVo.Container request_bodies { get ; set; }

    [Description (nick="headers")]
    public GVo.Container headers { get ; set; }

    [Description (nick="securitySchemes")]
    public GVo.Container security_schemes { get ; set; }

    [Description (nick="links")]
    public GVo.Container links { get ; set; }

    [Description (nick="callbacks")]
    public GVo.Container callbacks { get ; set; }

    [Description (nick="pathItems")]
    public GVo.Container path_items { get ; set; }
}
