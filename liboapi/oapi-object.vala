/* liboapi - OpenAPI implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Oapi.Object : GLib.Object, GVo.Object
{
    [Description (nick="openapi")]
    public string openapi { get ; set; default = "3.1"; }

    [Description (nick="info")]
    public Info info  { get ; set; }

    [Description (nick="jsonSchemaDialect")]
    public string json_schema_dialect { get ; set; default = "https://spec.openapis.org/oas/3.1/dialect/base"; }

    /**
     * List of {@link Server} object
     */
    public GVo.Container servers  { get ; set; }

    [Description (nick="components")]
    public Components components  { get ; set; }

    /**
     * List of {@link Security} object
     */
    [Description (nick="security")]
    public GVo.Container security  { get ; set; }

    /**
     * List of {@link Tag} object
     */
    [Description (nick="tags")]
    public GVo.Container tags  { get ; set; }

    [Description (nick="externalDocs")]
    public ExternalDocumentation external_docs  { get ; set; }
}
