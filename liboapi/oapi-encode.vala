/* liboapi - OpenAPI implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Oapi.Encode : GLib.Object, GVo.Object
{
    [Description (nick="contentType")]
    public string content_type { get ; set; }

    /**
     * A map of {@link Header} objects
     */
    [Description (nick="headers")]
    public GVo.Container headers { get ; set; }

    [Description (nick="style")]
    public string style { get ; set; }

    [Description (nick="explode")]
    public bool explode { get ; set; }

    [Description (nick="allowReserved")]
    public bool allow_reserved { get ; set; }
}
