/* liboapi - OpenAPI implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Oapi.Server : GLib.Object, GVo.Object
{
    [Description (nick="url")]
    public string url { get ; set; }

    [Description (nick="description")]
    public string description { get ; set; }

    /**
     * A container of key string-Variant dictionary 'a{sv}' objects like:
     *
     * [[[
     * "variables": {
     *   "username": {
     *     "default": "demo",
     *     "description": "this value is assigned by the service provider, in this example `gigantic-server.com`"
     *   },
     *   "port": {
     *     "enum": [
     *       "8443",
     *       "443"
     *     ],
     *    "default": "8443"
     *  },
     *  "basePath": {
     *    "default": "v2"
     *  }
     * }
     * ]]]
     */
    [Description (nick="variables")]
    public GVo.Container variables { get ; set; }
}
