/* liboapi - OpenAPI implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Oapi.MediaType : GLib.Object, GVo.Object
{
    [Description (nick="schema")]
    public Schema schema { get ; set; }

    [Description (nick="example")]
    public GLib.Variant example { get ; set; }

    /**
     * A map of {@link Example} objects
     */
    [Description (nick="examples")]
    public GVo.Container examples { get ; set; }

    /**
     * A map of {@link Encoding} objects
     */
    [Description (nick="encoding")]
    public GVo.Container encoding { get ; set; }
}
