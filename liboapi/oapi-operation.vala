/* liboapi - OpenAPI implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Oapi.Operation : GLib.Object, GVo.Object
{
    /**
     * A list of {@link GVo.String}
     */
    [Description (nick="tags")]
    public GVo.Container tags { get ; set; }

    [Description (nick="summary")]
    public string summary { get ; set; }

    [Description (nick="description")]
    public string description { get ; set; }

    [Description (nick="externalDocs")]
    public ExternalDocumentation external_docs { get ; set; }

    [Description (nick="operationId")]
    public string operation_id { get ; set; }

    /**
     * A list of {@link Parameter} or {@link Reference} objects
     */
    [Description (nick="parameters")]
    public GVo.Container parameters { get ; set; }

    /**
     * This could be: {@link RequestBody} or {@link Reference}
     */
    [Description (nick="requestBody")]
    public GLib.Variant request_body { get ; set; }

    [Description (nick="responses")]
    public Responses responses { get ; set; }

    /**
     * This could be: {@link Callback} or {@link Reference}
     */
    [Description (nick="callbacks")]
    public GVo.Container callbacks { get ; set; }

    [Description (nick="deprecated")]
    public bool deprecated { get ; set; }

    [Description (nick="security")]
    public SecurityRequirement security { get ; set; }

    /**
     * a List of {@link Server} objects
     */
    [Description (nick="servers")]
    public GVo.Container servers { get ; set; }
}
