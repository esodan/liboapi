/* liboapi - OpenAPI implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Oapi.OAuthFlow : GLib.Object, GVo.Object
{
    [Description (nick="authorizationUrl")]
    public string authorization_url { get ; set; }

    [Description (nick="tokenUrl")]
    public string token_url { get ; set; }

    [Description (nick="refreshUrl")]
    public string refresh_url { get ; set; }

    /**
     * A Map with a property string as a key an a {@link GVo.String}
     * object as a value
     */
    [Description (nick="scopes")]
    public GVo.Container scopes { get ; set; }
}
