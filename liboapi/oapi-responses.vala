/* liboapi - OpenAPI implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Oapi.Responses : GLib.Object, GVo.Object
{
    /**
     * This could be: {@link Response} or {@link Reference}
     */
    [Description (nick="default")]
    public GVo.Object default_response { get ; set; }
    /**
     * Objects with a property's name equal to the response code
     * with an object as a {@link Response} or {@link Reference}
     *
     * [[[
     * {
     *   "200": {
     *     "description": "a pet to be returned",
     *     "content": {
     *       "application/json": {
     *         "schema": {
     *           "$ref": "#/components/schemas/Pet"
     *         }
     *       }
     *     }
     *   },
     *   "default": {
     *     "description": "Unexpected error",
     *     "content": {
     *       "application/json": {
     *         "schema": {
     *           "$ref": "#/components/schemas/ErrorModel"
     *         }
     *       }
     *     }
     *   }
     * }
     *
     */
    public GVo.Container responses { get ; set; }
    // Requires a custom to/from Variant method
}
