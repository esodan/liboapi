/* liboapi - OpenAPI implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Oapi.Parameter : GLib.Object, GVo.Object
{
    [Description (nick="name")]
    public string name { get ; set; }

    [Description (nick="in")]
    public string in_type { get ; set; }

    [Description (nick="description")]
    public string description { get ; set; }

    [Description (nick="required")]
    public bool required { get ; set; }

    [Description (nick="deprecated")]
    public bool deprecated { get ; set; }

    /**
     * This property will be removed in the future
     */
    [Description (nick="allowEmptyValue")]
    public GVo.Boolean allow_empty_value { get ; set; }


    [Description (nick="style")]
    public string style { get ; set; }

    [Description (nick="explode")]
    public GVo.Boolean explode { get ; set; }

    [Description (nick="allowReserved")]
    public bool allow_reserved { get ; set; }

    /**
     * a {@link Schema} or {@link Reference} object
     */
    [Description (nick="schema")]
    public Schema schema { get ; set; }

    [Description (nick="example")]
    public GLib.Variant example { get ; set; }

    /**
     * a {@link Example} or {@link Reference} object
     */
    [Description (nick="examples")]
    public GVo.Container examples { get ; set; }

    /**
     * a Map with a string and {@link MediaType} objects
     * as value
     */
    [Description (nick="content")]
    public GVo.Container content { get ; set; }
}
